package com.epam.library;

import com.epam.library.entity.Author;
import com.epam.library.entity.Bookmark;
import com.epam.library.entity.Books;
import com.epam.library.entity.Users;
import com.epam.library.impdao.*;
import com.epam.library.manager.LocalizationManager;
import com.epam.library.manager.PropertiesManager;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import spark.Request;
import spark.Response;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static spark.Spark.*;

public class RestService {

	private Gson gson;
	private UsersDaoImp usersDao;
	private BookDaoImp bookDao;
	private AuthorDaoImp authorDaoImp;
	private BookmarkDaoImp bookmarkDaoImp;
	private static Logger logger = Logger.getLogger(RestService.class);
	private LocalizationManager localizationManager = LocalizationManager.getInstance();
	private PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private int port;
	private String ip;

	public RestService() {
		ip = propertiesManager.getValue("service.ip");
		if (propertiesManager.getValue("service.port").matches("\\d+")) {
			port = Integer.valueOf(propertiesManager.getValue("service.port"));
		} else {
			logger.error(localizationManager.getValue("failure.notcorrect.config.port"));
			port = 4567;
			logger.info(localizationManager.getValue("warning.config.default.port"));
		}
		port(port);
		ipAddress(ip);
		usersDao = new UsersDaoImp();
		bookDao = new BookDaoImp();
		authorDaoImp = new AuthorDaoImp();
		bookmarkDaoImp = new BookmarkDaoImp();
		gson = new Gson();
		try {
			logger.info("Service started. Use the url to go " + new URL("http://" + ip + ":" + port));
		} catch (MalformedURLException e) {
			logger.error(e);
		}


		before((req, res) -> {
			res.type("application/json");
			String token = req.queryParams("JWT");
			if (token == null) {
				token = auth(req, res);
				if (token == null) {
					halt(401, gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("access.registration"))));
				}
			} else {
				if (!Token.verifyToken(token)) {
					halt(401, gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("access.token.invalid"))));
				}
			}
		});


		path("/api", () -> {
			path("/books", () -> {
				get("", this::getBooksByMultiCriterion, new JsonTransformer());
				get("/", this::getBooksByMultiCriterion, new JsonTransformer());
				get("/:id", this::getBookById, new JsonTransformer());
				post("", this::createBook, new JsonTransformer());
				delete("", this::deleteBookById, new JsonTransformer());
			});
			path("/users", () -> {
				get("", this::getUsersByMultiCriterion, new JsonTransformer());
				get("/", this::getUsersByMultiCriterion, new JsonTransformer());
				get("/:id", this::getUserById, new JsonTransformer());
				post("", this::createUser, new JsonTransformer());
				post("/", this::createUser, new JsonTransformer());
				put("", this::setUserRole, new JsonTransformer());
				put("/", this::setUserRole, new JsonTransformer());
			});
			path("/bookmarks", () -> {
				post("", this::createBookmark, new JsonTransformer());
				post("/", this::createBookmark, new JsonTransformer());
				get("", this::getBookmarkByMultiCriterion, new JsonTransformer());
				get("/", this::getBookmarkByMultiCriterion, new JsonTransformer());
				get("/:id", this::getBookmarkById, new JsonTransformer());
				delete("", this::deleteBookmark, new JsonTransformer());
				delete("/", this::deleteBookmark, new JsonTransformer());
			});
			path("/authors", () -> {
				get("", this::getAuthorByMultiCriterion, new JsonTransformer());
				get("/", this::getAuthorByMultiCriterion, new JsonTransformer());
				get("/:id", this::getAuthorById, new JsonTransformer());
				post("", this::createAuthor, new JsonTransformer());
				post("/", this::createAuthor, new JsonTransformer());
			});
			post("/registration", this::registration);
			post("/auth", this::auth);
		});

	}

	private String setUserRole(Request req, Response res) {
		String userId = req.queryParams("userid");
		String roleId = req.queryParams("roleid");
		if (userId != null && userId.matches("\\d+")) {
			if (roleId != null && roleId.matches("\\d+")) {
				if (usersDao.findById(Integer.valueOf(userId)) != null) {
					if (usersDao.setRoleById(Integer.valueOf(userId), Integer.valueOf(roleId))) {
						res.status(200);
						return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.user.change.role")));
					} else {
						res.status(400);
						return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.user.change.role")));
					}
				} else {
					res.status(400);
					return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.user.id.notexist")));
				}
			} else {
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.user.role")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.user.id")));
		}
	}

	private String deleteBookmark(Request req, Response res) {
		String id = req.queryParams("id");
		Bookmark bookmark;
		if (id == null) {
			id = req.params("id");
		}
		if (null != id && id.matches("\\d+")) {
			bookmark = bookmarkDaoImp.findById(Integer.valueOf(id));
			if (bookmark.getUserId() == Token.getId(req.queryParams("JWT"))) {
				if (bookmarkDaoImp.delete(Integer.valueOf(id))) {
					res.status(200);
					return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.bookmark.delete")));
				} else {
					res.status(400);
					return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.bookmark.delete")));
				}
			}else{
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.role.havenot.access")));
			}
		} else {
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.args")));
		}
	}

	private String registration(Request req, Response res) {
		String login = req.queryParams("login");
		String password = req.queryParams("password");
		Users user = null;
		if (null != login && null != password) {
			user = new Users(login, password, 2);
			usersDao.create(user);
		}
		res.header("login", login);
		res.header("role", String.valueOf(user.getRole()));
		return auth(req, res);
	}

	private String auth(Request req, Response res) {
		String login = req.queryParams("login");
		String password = req.queryParams("password");
		String token = null;
		if (login == null || password == null) {
			halt(401, gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.args"))));
		}
		Users user = usersDao.findByLoginAndPassword(login, password);
		if (null != user) {
			token = Token.createToken(user.getLogin(), user.getRole(), user.getId());
		} else {
			halt(401, gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.auth.not.exist.account"))));
		}
		return "{\"JWT\":" + "\"" + token + "\"" + ",\"ok\": true}";
	}

	private String createAuthor(Request req, Response res) {
		String name = req.queryParams("name");
		if (null != name) {
			if (Token.getRole(req.queryParams("JWT")) != 1) {
				res.status(401);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.role.havenot.access")));
			} else {
				authorDaoImp.create(new Author(name));
				res.status(200);
				return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.author.add")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.author.add")));
		}
	}

	private String getAuthorByMultiCriterion(Request req, Response res) {
		String id = req.queryParams("id");
		String name = req.queryParams("name");
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(authorDaoImp.findById(Integer.valueOf(id)));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else if (null != name) {
			res.status(200);
			return gson.toJson(authorDaoImp.getAuthorByName(name));
		} else if (null == id && null == name) {
			res.status(200);
			return gson.toJson(authorDaoImp.findAll());
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("failure.notcorrect.author.id")));
		}
	}

	private String getAuthorById(Request req, Response res) {
		String id = req.queryParams("id");
		if (null == id) {
			id = req.params("id");
		}
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(authorDaoImp.findById(Integer.valueOf(id)));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.author.id")));
		}
	}

	private String createBookmark(Request req, Response res) {
		String bookid = req.queryParams("bookid");
		String page = req.queryParams("page");
		if (bookid.matches("\\d+") && page.matches("\\d+")) {
			try {
				if (bookmarkDaoImp.create(new Bookmark(Token.getId(req.queryParams("JWT")), Integer.valueOf(bookid), Integer.valueOf(page)))) {
					res.status(200);
					return gson.toJson(localizationManager.getValue("success.bookmark.add"));
				}
				res.status(400);
				return gson.toJson(localizationManager.getValue("failure.bookmark.add"));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(localizationManager.getValue("failure.notcorrect.bookmark.page") + " " +
					localizationManager.getValue("or") + " " +
					localizationManager.getValue("failure.notcorrect.bookmark.bookid"));
		}
	}

	private String getBookmarkByMultiCriterion(Request req, Response res) { //todo
		String id = req.queryParams("id");
		int userId = Token.getId(req.queryParams("JWT"));
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(bookmarkDaoImp.findById(Integer.valueOf(id)));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else if (0 != userId) {
			try {
				res.status(200);
				return gson.toJson(bookmarkDaoImp.findByUserId(Integer.valueOf(userId)));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else if (null == id) {
			res.status(200);
			return gson.toJson(bookmarkDaoImp.findAll());
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("failure.notcorrect.bookmark.id") + " " +
					localizationManager.getValue("or") + " "
					+ localizationManager.getValue("failure.notcorrect.bookmark.userid")));
		}
	}

	private String getBookmarkById(Request req, Response res) {
		if (req.params("id") != null && req.params("id").matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(bookmarkDaoImp.findById(Integer.valueOf(req.params("id"))));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.bookmark.id")));
		}
	}

	private String createUser(Request req, Response res) {
		String login = req.queryParams("login");
		String password = req.queryParams("password");
		String role = req.queryParamOrDefault("role", "2");
		if (null == login || null == password) {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.user.add")));
		} else if (role.matches("\\d+")) {
			if (!usersDao.create(new Users(login, password, Integer.valueOf(role)))) {
				res.status(400);
				return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("failure.user.add.exist")));
			} else {
				res.status(200);
				return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.user.add")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.user.add")));
		}
	}

	private String getUsersByMultiCriterion(Request req, Response res) {
		String role = req.queryParams("role");
		String id = req.queryParams("id");
		String login = req.queryParams("login");
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return getUserById(req, res);
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else if (login != null) {
			return gson.toJson(usersDao.findByLogin(login));
		} else if (null != role && role.matches("\\d+")) {
			res.status(200);
			return gson.toJson(usersDao.findByRole(Integer.valueOf(role)));
		} else if (null == role) {
			res.status(200);
			return gson.toJson(usersDao.findAll());
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.user.role")));
		}
	}

	private String getUserById(Request req, Response res) {
		String id = req.params("id");
		if (null == id) id = req.queryParams("id");
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(usersDao.findById(Integer.valueOf(id)));
			} catch (NumberFormatException e) {
				logger.error(e);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.user.id")));
		}
	}

	private String deleteBookById(Request req, Response res) {
		String id = req.queryParams("id");
		if (null != id && id.matches("\\d+")) {
			try {
				if (bookDao.findById(Integer.valueOf(id)) != null) {
					if (Token.getRole(req.queryParams("JWT")) != 1) {
						return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.role.havenot.access")));
					} else {
						if (bookDao.delete(Integer.valueOf(id))) {
							res.status(200);
							return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.book.delete")));
						} else {
							res.status(400);
							return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.book.delete")));
						}
					}
				} else {
					return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.book.notexist")));
				}
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.book.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.book.delete")));
		}
	}

	private String getBookById(Request req, Response res) {
		if (null != req.params("id") && req.params("id").matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(bookDao.findById(Integer.valueOf(req.params("id"))));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else {
			res.status(400);
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.book.id")));
		}
	}

	private String createBook(Request req, Response res) {
		String name = req.queryParams("name");
		String authorid = req.queryParams("authorid");
		String date = req.queryParams("date");
		boolean result = false;
		if (authorid != null && authorid.matches("\\d+") && name != null) {
			if (Token.getRole(req.queryParams("JWT")) != 1) {
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.role.havenot.access")));
			} else {
				if (date == null && !date.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d")) {
					result = bookDao.create(new Books(name, Integer.valueOf(req.queryParams("authorid"))));
				} else {
					result = bookDao.create(new Books(name, Integer.valueOf(authorid), date));
				}
				if (result) {
					res.status(200);
					return gson.toJson(Map.of("ok", true, "message", localizationManager.getValue("success.book.add")));
				} else {
					res.status(400);
					return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.book.add")));
				}
			}
		} else {
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.book.add.args")));
		}
	}

	private String getBooksByMultiCriterion(Request req, Response res) {
		String id = req.queryParams("id");
		if (null != id && id.matches("\\d+")) {
			try {
				res.status(200);
				return gson.toJson(bookDao.findById(Integer.valueOf(id)));
			} catch (NumberFormatException e) {
				logger.error(e);
				res.status(400);
				return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.incorrect.id")));
			}
		} else if (id != null) {
			return gson.toJson(Map.of("ok", false, "message", localizationManager.getValue("failure.notcorrect.book.id")));
		} else {
			String name = req.queryParams("name");
			String author = req.queryParams("author");
			String firstDate = req.queryParams("firstDate");
			String lastDate = req.queryParams("lastDate");
			String arr[] = new String[4];
			if (null == (arr[0] = name)) arr[0] = "";
			if (null == (arr[1] = author)) arr[1] = "";
			if (null == firstDate) {
				arr[2] = "";
			} else arr[2] = firstDate;
			if (null == lastDate) {
				arr[3] = "";
			} else arr[3] = lastDate;
			res.status(200);
			return gson.toJson(bookDao.getBookByAllCriterion(arr));
		}
	}
}
