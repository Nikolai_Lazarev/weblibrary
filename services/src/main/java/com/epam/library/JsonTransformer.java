package com.epam.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {
	@Override
	public String render(Object o) throws Exception {
		JsonParser jsonParser = new JsonParser();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String jsonString = o.toString();
		JsonElement el = jsonParser.parse(jsonString);
		return gson.toJson(el);
	}
}
