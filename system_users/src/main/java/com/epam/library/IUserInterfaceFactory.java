package com.epam.library;

public interface IUserInterfaceFactory {
	ISystemUserInterface createInterface(int id, TCPConnection tcpConnection);
}
