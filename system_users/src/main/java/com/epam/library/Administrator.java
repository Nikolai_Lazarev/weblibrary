package com.epam.library;

import java.io.IOException;

public class Administrator extends User {

	private ISystemUserInterface userInterface;


	public Administrator(int id, String login, IUserInterfaceFactory userInterfaceFactory) throws IOException {
		super(id, login, userInterfaceFactory);
	}



	@Override
	public void onConnectionReady(TCPConnection connection) {

	}

	@Override
	public void onMessage(TCPConnection connection, byte[] message) {
		printMessage(message);
		System.out.println("-------------------------------");
	}


	@Override
	public void onDisconnection(TCPConnection connection) {

	}


}
