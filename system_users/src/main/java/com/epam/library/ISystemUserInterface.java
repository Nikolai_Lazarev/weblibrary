package com.epam.library;

import com.epam.library.manager.UserQuery;

public interface ISystemUserInterface {
	void printCommandList();
	UserQuery doCommand(String command);
}
