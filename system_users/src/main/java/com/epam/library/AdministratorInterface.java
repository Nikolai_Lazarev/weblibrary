package com.epam.library;

import com.epam.library.manager.QueryType;
import com.epam.library.manager.UserQuery;
import org.apache.log4j.Logger;

import java.util.Scanner;
import java.util.regex.Matcher;

public class AdministratorInterface extends UserInterface implements ISystemUserInterface {

	private static Logger logger = Logger.getLogger(AdministratorInterface.class);

	public AdministratorInterface(int id, TCPConnection tcpConnection){
		super(id, tcpConnection);
	}

	@Override
	public void printCommandList() {
		super.printCommandList();
		String commandList =
				"4: Добавить книгу\n" +
						"5: Удалить книгу\n" +
						"6: Назначить роль\n" +
						"7: Добавить пользователя";
		System.out.println(commandList);
	}

	@Override
	public UserQuery doCommand(String command) {
		UserQuery query;
		if ((query = super.doCommand(command)) != null) {
			return query;
		} else
			switch (command.trim()) {
				case "4":
					return addBook();
				case "5":
					return deleteBook();
				case "6":
					return setRole();
				case "7":
					return addUser();
				default:
					return null;

			}
	}

	private UserQuery getUsersList(){
		return new UserQuery(QueryType.GET_USERS_LIST);
	}

	private UserQuery addBook() {
		String args[] = new String[4];
		Scanner scanner = new Scanner(System.in);
		System.out.println("Введите название книги или -1 для выхода");
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		while(args[0].trim().equals("")){
			System.out.println("Введите название книги");
			args[0] = scanner.nextLine();
		}
		System.out.println("Введите имя автора");
		args[1] = scanner.nextLine();
		System.out.println("Введите год выпуска в формате XXXX-XX-XX");
		args[2] = scanner.nextLine();
		Matcher matcher = propertiesManager.getDatePattern().matcher(args[2]);
		while(!matcher.matches()){
			args[2] = scanner.nextLine();
			matcher = propertiesManager.getDatePattern().matcher(args[2]);
		}
		System.out.println("Добавить автора если такого нет ? Y/N");
		String input = "";
		while(!("y".equalsIgnoreCase(input)||"n".equalsIgnoreCase(input))){
			input = scanner.nextLine();
		}
		if("n".equalsIgnoreCase(input)){
			args[3] = "0";
		}else args[3] = "1";
		for(String x : args){
			System.out.println(x);
		}
		return new UserQuery(QueryType.ADD_BOOK, args);
	}

	private UserQuery deleteBook() {
		System.out.println("Введите ISBN или -1 для выхода");
		String args[] = new String[1];
		Scanner scanner = new Scanner(System.in);
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		return new UserQuery(QueryType.DELETE_BOOK, args);
	}

	private UserQuery setRole() {
		tcpConnection.sendMessage(getUsersList());
		String args[] = new String[2];
		Scanner scanner = new Scanner(System.in);
		logger.info("Введите id пользователя или -1 для выхода");
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		logger.info("Введите id роли");
		args[1] = scanner.nextLine();
		return new UserQuery(QueryType.SET_ROLE, args);
	}

	private UserQuery addUser() {
		String args[] = new String[3];
		Scanner scanner = new Scanner(System.in);
		logger.info("Введите логин или -1 для выхода");
		args[0] = scanner.nextLine();
		if ("-1".equals(args[0])) return null;
		logger.info("Введите пароль");
		args[1] = scanner.nextLine();
		logger.info("Введите роль");
		args[2] = scanner.nextLine();
		return new UserQuery(QueryType.ADD_USER, args);
	}
}
