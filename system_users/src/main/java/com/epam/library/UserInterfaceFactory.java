package com.epam.library;

public class UserInterfaceFactory implements IUserInterfaceFactory {

	@Override
	public ISystemUserInterface createInterface(int id, TCPConnection tcpConnection) {
		return new UserInterface(id, tcpConnection);
	}
}
