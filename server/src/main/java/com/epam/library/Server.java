package com.epam.library;


import com.epam.library.connection_pool.ConnectionPool;
import com.epam.library.impdao.BookDaoImp;
import com.epam.library.manager.PropertiesManager;
import com.epam.library.manager.UserQuery;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;


public class Server implements ITCPConnectionListener {


	private static ArrayList<TCPConnection> connections;
	private static Logger logger = Logger.getLogger(Server.class);
	private static ServerSocket serverSocket;
	private static PropertiesManager manager;
	private static ConnectionPool connectionPool;
	private static BookDaoImp bookDaoImp;

	private Server() {
		logger.info("Server running...");
		connections = new ArrayList<TCPConnection>();
		connectionPool = ConnectionPool.getInstance();
		bookDaoImp = new BookDaoImp();
		manager = PropertiesManager.getInstance();
		serverSocket = null;
		try {
			serverSocket = new ServerSocket(manager.getPort());
			while (true) {
				new TCPConnection(serverSocket.accept(), this);
			}
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static void main(String[] args) {
		new Server();
	}

	@Override
	public void onConnectionReady(TCPConnection connection) {
		connections.add(connection);
	}

	@Override
	public void onMessage(TCPConnection connection, byte[] message) {
		if (SerializationUtils.deserialize(message) instanceof UserQuery) {
			UserQuery userQuery = SerializationUtils.deserialize(message);
			connection.sendMessage(DoQuery.doThisQuery(userQuery));
		}
	}

	@Override
	public void onDisconnection(TCPConnection connection) {
		connections.remove(connection);
		connection.disconnection();
	}

	private void disconnectAllConnections() {
		for (TCPConnection x : connections) {
			x.disconnection();
		}
	}

	private void removeAllConnections() {
		for (TCPConnection x : connections) {
			connections.remove(x);
		}
	}

}
