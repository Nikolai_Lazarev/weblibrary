package com.epam.library;

import com.epam.library.entity.Author;
import com.epam.library.entity.Bookmark;
import com.epam.library.entity.Books;
import com.epam.library.entity.Users;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IService {
	Books getBookById(int id);
	List<Books> getBooks();
	boolean deleteBookById(int id);
	boolean addBook(Books book);
	Books getBookByName(String name);
	Books getBookByDate(String date);

	Author getAuthorById(int id);
	List<Author> getAuthorByName(String name);
	boolean addAuthor(Author author);

	Bookmark getBookmarkById(int id);
	List<Bookmark> getBookmarksByUserId(int userId);
	boolean addBookmark(Bookmark bookmark);
	boolean deleteBookmarkById(int id);

	Users getUserById(int id);
	List<Users> getUsersByRole(int roleId);
	boolean createUser(Users user);
}
