package com.epam.library;

import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import javax.xml.ws.Endpoint;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private static Logger logger = Logger.getLogger(Main.class);

	public static void main(String[] args)  {
		String address = propertiesManager.getValue("service.ip") + ":" + propertiesManager.getValue("service.soap.port") + "/library";
		Endpoint.publish("http://" + address, new Service());
		try {
			logger.info(new URL(address));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}
}
