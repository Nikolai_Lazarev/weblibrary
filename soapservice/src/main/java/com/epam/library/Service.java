package com.epam.library;

import com.epam.library.entity.Author;
import com.epam.library.entity.Bookmark;
import com.epam.library.entity.Books;
import com.epam.library.entity.Users;
import com.epam.library.impdao.AuthorDaoImp;
import com.epam.library.impdao.BookDaoImp;
import com.epam.library.impdao.BookmarkDaoImp;
import com.epam.library.impdao.UsersDaoImp;

import javax.jws.WebService;
import java.util.List;

@WebService
public class Service implements IService {
	private BookDaoImp bookDaoImp;
	private BookmarkDaoImp bookmarkDaoImp;
	private UsersDaoImp usersDaoImp;
	private AuthorDaoImp authorDaoImp;

	public Service() {
		this.bookDaoImp = new BookDaoImp();
		this.bookmarkDaoImp = new BookmarkDaoImp();
		this.usersDaoImp = new UsersDaoImp();
		this.authorDaoImp = new AuthorDaoImp();
	}

	@Override
	public Books getBookById(int id) {
		return bookDaoImp.findById(id);
	}

	@Override
	public List<Books> getBooks() {
		return bookDaoImp.findAll();
	}

	@Override
	public boolean deleteBookById(int id) {
		return bookDaoImp.delete(id);
	}

	@Override
	public boolean addBook(Books book) {
		return bookDaoImp.create(book);
	}

	@Override
	public Books getBookByName(String name) {
		return bookDaoImp.getBookByName(name);
	}

	@Override
	public Books getBookByDate(String date) {
		return getBookByDate(date);
	}

	@Override
	public Author getAuthorById(int id) {
		return authorDaoImp.findById(id);
	}

	@Override
	public List<Author> getAuthorByName(String name) {
		return authorDaoImp.getAuthorByName(name);
	}

	@Override
	public boolean addAuthor(Author author) {
		return authorDaoImp.create(author);
	}

	@Override
	public Bookmark getBookmarkById(int id) {
		return bookmarkDaoImp.findById(id);
	}

	@Override
	public List<Bookmark> getBookmarksByUserId(int userId) {
		return bookmarkDaoImp.findByUserId(userId);
	}

	@Override
	public boolean addBookmark(Bookmark bookmark) {
		return bookmarkDaoImp.create(bookmark);
	}

	@Override
	public boolean deleteBookmarkById(int id) {
		return bookmarkDaoImp.delete(id);
	}

	@Override
	public Users getUserById(int id) {
		return usersDaoImp.findById(id);
	}

	@Override
	public List<Users> getUsersByRole(int roleId) {
		return usersDaoImp.findByRole(roleId);
	}

	@Override
	public boolean createUser(Users user) {
		return usersDaoImp.create(user);
	}
}
