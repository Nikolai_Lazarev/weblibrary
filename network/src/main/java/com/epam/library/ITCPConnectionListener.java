package com.epam.library;

public interface ITCPConnectionListener {

	void onConnectionReady(TCPConnection connection);
	void onMessage(TCPConnection connection, byte [] message);
	void onDisconnection(TCPConnection connection);

}
