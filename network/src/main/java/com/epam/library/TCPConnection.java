package com.epam.library;

import com.epam.library.manager.UserQuery;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class TCPConnection {


	private static Logger logger = Logger.getLogger(TCPConnection.class);
	private Socket socket;
	private DataInputStream in;
	private DataOutputStream out;
	private Thread rxThread;
	private ITCPConnectionListener connectionListener;


	public TCPConnection(String ipAddr, int portAddr, ITCPConnectionListener connectionListener) throws IOException {
		this(new Socket(ipAddr, portAddr), connectionListener);
	}

	public TCPConnection(TCPConnection connection){

	}

	public TCPConnection(Socket socket, final ITCPConnectionListener connectionListener) throws IOException {
		this.socket = socket;
		this.connectionListener = connectionListener;
		in = new DataInputStream(socket.getInputStream());
		out = new DataOutputStream(socket.getOutputStream());
		rxThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					connectionListener.onConnectionReady(TCPConnection.this);
					while (!rxThread.isInterrupted()) {
						connectionListener.onMessage(TCPConnection.this, readBytes());
					}
				} catch (IOException e) {
					logger.error(e);
				} finally {
					connectionListener.onDisconnection(TCPConnection.this);
				}
			}
		});
		rxThread.start();
	}

	public DataInputStream getIn() {
		return in;
	}

	public void disconnection() {
		try {
			rxThread.interrupt();
			socket.close();
		} catch (IOException e) {
			logger.error(e);
		}
	}

	private void sendBytes(byte[] myByteArray, int start, int len) throws IOException {
		if (len < 0)
			throw new IllegalArgumentException("Negative length not allowed");
		if (start < 0 || start >= myByteArray.length)
			throw new IndexOutOfBoundsException("Out of bounds: " + start);
		out.writeInt(len);
		if (len > 0) {
			out.write(myByteArray, start, len);
			out.flush();
		}
	}

	public byte[] readBytes() throws IOException {
		int len = in.readInt();
		byte[] data = new byte[len];
		if (len > 0) {
			in.readFully(data);
		}
		return data;
	}


	public void sendMessage(UserQuery object) {
		try {
			byte[] data = SerializationUtils.serialize(object);
			sendBytes(data, 0, data.length);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public void sendMessage(List<IExecuteQuery> object) {
		try {
			for (IExecuteQuery x : object) {
				byte[] data = SerializationUtils.serialize(x);
				sendBytes(data, 0, data.length);
			}
		} catch (IOException e) {
			logger.error(e);
		}
	}
}
