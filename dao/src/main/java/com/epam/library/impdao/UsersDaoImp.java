package com.epam.library.impdao;


import com.epam.library.connection_pool.ConnectionPool;
import com.epam.library.entity.Users;
import com.epam.library.idao.IUsersDAO;
import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsersDaoImp implements IUsersDAO {


	private static ConnectionPool connectionPool = ConnectionPool.getInstance();
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private static Logger logger = Logger.getLogger(UsersDaoImp.class);


	@Override
	public ArrayList<Users> findByRole(int role) {
		Connection connection = null;
		ArrayList<Users> usersList = new ArrayList<Users>();
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from Users where role = ?")) {
				statement.setInt(1, role);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						usersList.add(new Users(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("role")));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return usersList;
	}

	@Override
	public Users findByLoginAndPassword(String login, String password) {
		Connection connection = null;
		Users user = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from Users where password = md5(?) and login = ?")) {
				statement.setString(1, password);
				statement.setString(2, login);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						user = new Users(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("role"));
					}
				}
			}
		} catch (SQLException e) {
			logger.info(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return user;

	}


	@Override
	public List<Users> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ArrayList<Users> usersList = new ArrayList<Users>();
		try {
			connection = connectionPool.getConnection();
			statement = connection.prepareStatement("Select * from Users");
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					usersList.add(new Users(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("role")));
				}
			}
		} catch (SQLException e) {
			logger.error(e);;
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}

		return usersList;
	}


	@Override
	public boolean delete(Integer id) {
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("delete from Users where id = ?")) {
				statement.setInt(1, id);
				statement.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			logger.error(e);
			return false;
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
	}

	@Override
	public boolean create(Users users) {
		Connection connection = null;
		if(null == findByLoginAndPassword(users.getLogin(), users.getPassword())) {
			try {
				connection = connectionPool.getConnection();
				try (PreparedStatement statement = connection.prepareStatement("insert into Users (login, password, role) values (?, ?, ?)")) {
					statement.setString(1, users.getLogin());
					statement.setString(2, users.getPassword());
					statement.setInt(3, users.getRole());
					statement.executeUpdate();
					return true;
				}
			} catch (SQLException e) {
				logger.error(e);
				return false;
			} finally {
				logger.info("return connection...");
				connectionPool.putConnection(connection);
			}
		}else return false;
	}
	
	public List<Users> findByLogin(String login){
		Connection connection = null;
		List<Users> user = new ArrayList<>();
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from Users where login like ?")) {
				statement.setString(1, '%'+login+'%');
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						user.add(new Users(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("role")));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return user;
	}

	@Override
	public Users findById(Integer id) {
		Connection connection = null;
		Users user = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from Users where id = ?")) {
				statement.setInt(1, id);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						user = new Users(resultSet.getInt("id"), resultSet.getString("login"), resultSet.getInt("role"));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return user;
	}

	public boolean setRoleById(int userid, int roleId) {
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("update users set role = ? where id = ?")) {
				statement.setInt(1, roleId);
				statement.setInt(2, userid);
				statement.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return false;
	}


	@Override
	public void close(Statement statement) {

	}

	@Override
	public void close(Connection connection) {

	}
}
