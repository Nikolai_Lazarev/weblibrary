package com.epam.library.impdao;

import com.epam.library.connection_pool.ConnectionPool;
import com.epam.library.entity.Author;
import com.epam.library.idao.IAuthorDAO;
import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDaoImp implements IAuthorDAO {

	private static ConnectionPool connectionPool = ConnectionPool.getInstance();
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private static Logger logger = Logger.getLogger(AuthorDaoImp.class);

	@Override
	public List<Author> getAuthorByName(String name) {
		Connection connection = null;
		List<Author> author = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from authors where name like ?")) {
				statement.setString(1, '%'+name+'%');
				try (ResultSet resultSet = statement.executeQuery()) {
					author = new ArrayList<Author>();
					while (resultSet.next()) {
						author.add(new Author(resultSet.getInt("id"), resultSet.getString("name")));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return author;
	}

	@Override
	public List<Author> findAll() {
		Connection connection = null;
		ArrayList<Author> authors = null;
		Author author = null;
		try {
			authors = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from authors")) {
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						authors.add(new Author(resultSet.getInt("id"), resultSet.getString("name")));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return authors;
	}

	@Override
	public boolean delete(Integer id) {
		return false;
	}

	@Override
	public boolean create(Author author) {
		Connection connection = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("insert into authors (name) values (?)")) {
				statement.setString(1, author.getName());
				statement.executeUpdate();
				return true;
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return false;
	}

	@Override
	public Author findById(Integer id) {
		Connection connection = null;
		ResultSet resultSet = null;
		Author author = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("Select * from authors where id = ?")) {
				statement.setInt(1, id);
				resultSet = statement.executeQuery();
				while (resultSet.next()) {
					author = new Author(id, resultSet.getString("name"));
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return author;
	}
}
