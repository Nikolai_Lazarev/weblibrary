package com.epam.library.impdao;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.util.Map;

public class Token {
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();
	private static Logger logger = Logger.getLogger(Token.class);

	public static String createToken(String login, int role, int id) {
		try {
			return JWT.create()
					.withIssuer("library")
					.withClaim("login", login)
					.withClaim("role", role)
					.withClaim("ID", id)
					.sign(Algorithm.HMAC256(propertiesManager.getValue("secret")));
		} catch (JWTCreationException e) {
			logger.error(e);
		}
		return null;
	}

	public static Boolean verifyToken(String token) {
		boolean status = false;
		try {
			Algorithm algorithm = Algorithm.HMAC256(propertiesManager.getValue("secret"));
			JWTVerifier verifier = JWT.require(algorithm)
					.withIssuer("library")
					.build();
			DecodedJWT jwt = verifier.verify(token);
			status = true;
		} catch (JWTCreationException e) {
			logger.error(e);
			status = false;
		} finally {
			return status;
		}
	}

	static public int getId(String token) {
		DecodedJWT jwt = JWT.decode(token);
		return jwt.getClaim("ID").asInt();
	}
	static public int getRole(String token){
		DecodedJWT jwt = JWT.decode(token);
		return jwt.getClaim("role").asInt();
	}
}
