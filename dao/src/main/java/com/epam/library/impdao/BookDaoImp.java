package com.epam.library.impdao;

import com.epam.library.connection_pool.ConnectionPool;
import com.epam.library.entity.Books;
import com.epam.library.idao.IBooksDAO;
import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImp implements IBooksDAO {

	private static ConnectionPool connectionPool = ConnectionPool.getInstance();
	private static Logger logger = Logger.getLogger(BookmarkDaoImp.class);
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();

	@Override
	public Books getBookByName(String name) {
		Connection connection = null;
		Books book = null;
		ResultSet resultSet;
		PreparedStatement statement = null;
		try {
			connection = connectionPool.getConnection();
			statement = connection.prepareStatement("select * from books where name = ? and active = 1");
			statement.setString(1, name);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				book = new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
						resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString(), resultSet.getBoolean("active"));
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return book;
	}

	public List<Books> searchBookByDate(String[] args) {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where issueYear > ? and issueYear < ? and active = 1")) {
				statement.setString(1, args[0]);
				statement.setString(2, args[1]);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						books.add(new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
								resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString(), resultSet.getBoolean("active")));
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			if (connection != null) {
				connectionPool.putConnection(connection);
			}
		}
		return books;
	}

	private PreparedStatement beforeDate(String date) {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where issueYear < ?")) {
				statement.setString(1, date);
				return statement;
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			if (connection != null) {
				connectionPool.putConnection(connection);
			}
		}
		return null;
	}

	private PreparedStatement afterDate(String date) {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where issueYear > ?")) {
				statement.setString(1, date);
				return statement;
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			if (connection != null) {
				connectionPool.putConnection(connection);
			}
		}
		return null;
	}


	@Override
	public List<Books> getBookByAllCriterion(String[] args) {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		if (args[2].isEmpty() || null == args[2]) {
			args[2] = "0001-01-01";
		}
		if (args[3].isEmpty() || null == args[3]) {
			args[3] = "9999-01-01";
		}
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			String query;


			try (PreparedStatement statement = connection.prepareStatement("select * from books where " +
					"name like ? and authorId in (select id from authors where name like ?) and issueYear >= ? and issueYear <= ? and active = 1")) {
				statement.setString(1, '%' + args[0] + '%');
				statement.setString(2, '%' + args[1] + '%');
				statement.setString(3, args[2]);
				statement.setString(4, args[3]);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						if (resultSet.getDate("issueYear") != null) {
							books.add(new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
									resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString()));
						} else {
							books.add(new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
									resultSet.getInt("authorId")));
						}
					}
				}
			}

		} catch (SQLException e) {
			logger.error(e);
		} finally {
			if (connection != null) {
				connectionPool.putConnection(connection);
			}
		}
		return books;
	}

	@Override
	public List<Books> getBookByAuthor(String name) {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where authorId in (Select id from authors where name = ?) and active = 1")) {
				statement.setString(1, name);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						book = new Books(resultSet.getInt("ISBN"), name,
								resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString(), resultSet.getBoolean("active"));
						books.add(book);
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return books;
	}

	@Override
	public List<Books> findAll() {
		Connection connection = null;
		Books book = null;
		ArrayList<Books> books = null;
		try {
			books = new ArrayList<>();
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where active = 1");
			     ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					if (null != resultSet.getDate("issueYear")) {
						book = new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
								resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString(), resultSet.getBoolean("active"));
						books.add(book);
					} else {
						book = new Books(resultSet.getInt("ISBN"), resultSet.getString("name"),
								resultSet.getInt("authorId"));
						books.add(book);
					}
				}
			}

		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return books;
	}

	@Override
	public boolean delete(Integer id) {
		Connection connection = null;
		if (findById(id) != null) {
			try {
				connection = connectionPool.getConnection();
				try (PreparedStatement statement = connection.prepareStatement("update books set active = 0 where ISBN = ? ")) {
					statement.setInt(1, id);
					statement.executeUpdate();
					return true;
				}
			} catch (SQLException e) {
				logger.error(e);
			} finally {
				logger.info("return connection...");
				connectionPool.putConnection(connection);
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public boolean create(Books books) {
		Connection connection = null;
		boolean status = false;
		try {
			connection = connectionPool.getConnection();
			String date = books.getIssueYear();
			if (date == null) {
				date = "2020-01-01";
			}
			try (PreparedStatement statement = connection.prepareStatement("insert into books (name, authorId, issueYear, active) values (?, ?, ?, true)")) {
				statement.setString(1, books.getName());
				statement.setInt(2, books.getAuthor());
				statement.setString(3, date);
				statement.execute();
				status =  true;
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return status;
	}

	@Override
	public Books findById(Integer id) {
		Connection connection = null;
		Books book = null;
		try {
			connection = connectionPool.getConnection();
			try (PreparedStatement statement = connection.prepareStatement("select * from books where ISBN = ? and active = 1")) {
				statement.setInt(1, id);
				try (ResultSet resultSet = statement.executeQuery()) {
					while (resultSet.next()) {
						if (resultSet.getDate("issueYear") == null) {
							book = new Books(id, resultSet.getString("name"), resultSet.getInt("authorId"), "Unknown",
									resultSet.getBoolean("active"));
						} else {
							book = new Books(id, resultSet.getString("name"), resultSet.getInt("authorId"), resultSet.getDate("issueYear").toString(),
									resultSet.getBoolean("active"));
						}
					}
				}
			}
		} catch (SQLException e) {
			logger.error(e);
		} finally {
			logger.info("return connection...");
			connectionPool.putConnection(connection);
		}
		return book;
	}
}
