package com.epam.library.idao;


import com.epam.library.entity.Author;

import java.util.List;

public interface IAuthorDAO extends ILibraryDAO<Integer , Author> {
	List<Author> getAuthorByName(String name);
}
