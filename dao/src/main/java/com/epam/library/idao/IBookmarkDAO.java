package com.epam.library.idao;

import com.epam.library.entity.Bookmark;

public interface IBookmarkDAO extends ILibraryDAO<Integer, Bookmark> {
}
