package com.epam.library.idao;

import com.epam.library.entity.Books;

import java.util.ArrayList;
import java.util.List;

public interface IBooksDAO extends ILibraryDAO<Integer, Books> {
		Books getBookByName(String name);
		List<Books> getBookByAllCriterion(String args[]);
		List<Books> getBookByAuthor(String name);
}
