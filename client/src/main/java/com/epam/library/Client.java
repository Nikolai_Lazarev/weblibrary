package com.epam.library;

import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class Client
{

    private static Logger logger = Logger.getLogger(Client.class);


    public static void main( String[] args )
    {
        try {
           new NotAuthorized();
        }catch (IOException e){
            logger.error(e);
        }
    }
}
