package com.epam.library.entity;

public class Bookmark extends Entity {
	private int id;
	private int bookId;
	private int userId;
	private int page;
	private boolean active;

	public Bookmark(int userId, int bookId, int page) {
		this.bookId = bookId;
		this.userId = userId;
		this.page = page;
		this.active = true;
	}

	public Bookmark(int id, int userId, int bookId, int page, boolean state) {
		this.bookId = bookId;
		this.id = id;
		this.userId = userId;
		this.page = page;
		this.active = state;
	}

	public int getBookId() {
		return bookId;
	}

	public int getUserId() {
		return userId;
	}

	public int getPage() {
		return page;
	}

	public boolean isActive() {
		return active;
	}

	@Override
	public String toString() {
		return "ISBN книги: " + bookId +
				"\nСтраница: " + page +
				"\nАктивна: " + active;
	}
}
