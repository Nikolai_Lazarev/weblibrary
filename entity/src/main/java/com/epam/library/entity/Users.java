package com.epam.library.entity;

public class Users extends Entity {
	private int id;
	private String login;
	private String password;
	private int role;

	public Users(String login, String password, int role) {
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public Users(int id, String login, int role) {
		this.id = id;
		this.login = login;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public int getRole() {
		return role;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRole(int role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Id: " + id + "\nЛогин: " + login + "\nРоль: " + role;
	}
}
