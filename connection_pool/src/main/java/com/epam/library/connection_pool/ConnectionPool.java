package com.epam.library.connection_pool;

import com.epam.library.manager.PropertiesManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;

public class ConnectionPool {

	private ArrayDeque<Connection> pool;
	private static final int POOL_CAPACITY = 5;
	private static Logger logger = Logger.getLogger(ConnectionPool.class);
	private static ConnectionPool instance;
	private static PropertiesManager propertiesManager = PropertiesManager.getInstance();

	private ConnectionPool() {
		pool = new ArrayDeque<>(POOL_CAPACITY);
		addConnections(POOL_CAPACITY);
	}

	private void addConnections(int capacity) {
		for (int i = 0; i < capacity; i++) {
			pool.add(createConnection());
		}
	}

	private Connection createConnection() {
		Connection connection = null;
		try {
			logger.info("Create connection...");
			connection = DriverManager.getConnection(propertiesManager.getDb(), propertiesManager.getUser(), propertiesManager.getPassword());

		} catch (SQLException e) {
			logger.error(e);
		}
		return connection;
	}

	public Connection getConnection() {
		logger.info("getting connection...");
		if (pool.size() == 0) {
			addConnections(2);
		}
		return pool.removeLast();
	}

	public void putConnection(Connection connection) {
		pool.add(connection);
		if (pool.size() > 5) removeConnections();
	}

	private void removeConnections() {
		int poolSize = pool.size();
		while (poolSize > 5) {
			pool.removeLast();
			poolSize--;
		}
	}

	public static ConnectionPool getInstance() {
		if (null == instance) {
			instance = new ConnectionPool();
		}
		return instance;
	}

	public void closeAllConnections() {
		for (Connection x : pool) {
			try {
				x.close();
			} catch (SQLException e) {
				logger.error(e);
			}
		}
	}
}
