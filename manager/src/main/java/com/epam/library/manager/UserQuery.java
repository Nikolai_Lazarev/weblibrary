package com.epam.library.manager;

import java.io.Serializable;

public class UserQuery implements Serializable {

	private QueryType type;
	private String [] args;

	public UserQuery(QueryType type, String ... args){
		this.type = type;
		this.args = args;
	}

	public UserQuery(QueryType type){
		this.type = type;
	}

	public QueryType getType() {
		return type;
	}

	public String[] getArgs() {
		return args;
	}
}
