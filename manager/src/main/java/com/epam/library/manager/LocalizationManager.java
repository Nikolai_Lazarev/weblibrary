package com.epam.library.manager;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class LocalizationManager {

	private static Properties properties = new Properties();
	private static LocalizationManager instance;
	private Language lang;
	private static Logger logger = Logger.getLogger(LocalizationManager.class);

	private LocalizationManager() {
		if (null == lang) {
			lang = Language.ENGLISH;
		}
		try {
			switch (lang) {
				case ENGLISH:
					properties.load(getClass().getClassLoader().getResourceAsStream("English.properties"));
					break;
				case RUSSIAN:
					properties.load(getClass().getClassLoader().getResourceAsStream("Russian.properties"));
					break;
				default:
			}
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static LocalizationManager getInstance() {
		if (null == instance) {
			instance = new LocalizationManager();
		}
		return instance;
	}

	public void setLanguage(Language lang) {
		this.lang = lang;
	}

	public String getValue(String key) {
		return properties.getProperty(key);
	}

}
